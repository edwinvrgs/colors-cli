const yargs = require('yargs');

const { getColor } = require('./apiMock');

function getColors(colors) {
	return colors.map(getColor);
}

// Program API
const argv = yargs
	.options({
		'colors': {
			alias: 'c',
			describe: 'Return the selected colors in the selected order',
			default: 'all',
			array: true
		},
		'format': {
			alias: 'f',
			describe: 'Format of the colors returned',
			default: 'hex',
			choices: ['hex', 'rgb']
		},
		'type': {
			alias: 't',
			describe: 'Type of run',
			default: 'async',
			choices: ['sync', 'async']
		},
	})
	.help()
	.alias('help', 'h')
	.argv;

const getFormattedColor = (color, format) => {
	return format === 'rgb' ? color.RGB : color.HEX
};

function main() {
	if (argv.type === 'sync') {
		argv.colors.reduce( async (previousPromise, colorName) => {
			await previousPromise;
			return getColor(colorName).then(color => console.log(getFormattedColor(color, argv.format)));
		}, Promise.resolve());
	} else {
		const colorsPromises = getColors(argv.colors)
		Promise.all(colorsPromises).then(colors => console.log(colors.map(color => getFormattedColor(color, argv.format))));
	}
}

main();
